import { Router } from "express";

const router = Router();

router.get("/", (req, res) => {
  res.status(200).send("Hello Express!");
});

router.get("/users", (req, res) => {
  res.status(200).send("Get all users");
});

router.get("/query", (req, res) => {
  const { name, age } = req.query;
  res.status(200).send(`I am ${name} and i am ${age} years old.`);
});

router.get("/params/:id", (req, res) => {
  const { id } = req.params;
  res.status(200).send(`My id is ${id}.`);
});

router.post("/body", (req, res) => {
  const { favAnimal, amount } = req.body;
  res
    .status(200)
    .send(`My favorite animal is ${favAnimal}, and I have ${amount} of them.`);
});

router.get("/html-markup", (req, res) => {
  const { name } = req.query;
  res.status(200).send(`<h1>My name is in the header: ${name}</h1>`);
});

export default router;
