import { Router } from "express";
import usersRouter from "./users.router.js";

const router = Router();

router.use("/users", usersRouter);

router.get("/json-response", (req, res) => {
  res.status(200).json(req.query.food);
});

export default router;
