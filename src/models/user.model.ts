import fs from "fs/promises";
import path from "path";
import { IUser } from "../types/user.interface";

const USER_DB_PATH = path.join(process.cwd(), "json-db", "users.json");

class UserModel {
  constructor(private readonly _filePath: string) {}

  async save(data: IUser[]) {
    await fs.writeFile(this._filePath, JSON.stringify(data), "utf-8");
  }

  async read(): Promise<IUser[]> {
    try {
      const data = await fs.readFile(this._filePath, "utf-8");
      return JSON.parse(data);
    } catch (err) {
      return [];
    }
  }
}

const instance = new UserModel(USER_DB_PATH);

export default instance;
