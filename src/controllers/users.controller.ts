import { RequestHandler } from "express";
import { InternalServerException } from "../exceptions/internalServer.exception.js";
import { ResponseMessage } from "../types/messages.interface.js";
import userService from "../services/users.service.js";
import { UrlNotFoundException } from "../exceptions/urlNotFound.exception.js";
import { ICreateUserDto, IUpdateUserDto } from "../types/user.interface.js";

export const getAllUsers: RequestHandler = async (req, res, next) => {
  try {
    const users = await userService.getAllUsers();

    const response: ResponseMessage = {
      status: 200,
      message: "success",
      data: { users },
    };

    res.status(response.status).json(response);
  } catch (err) {
    next(new InternalServerException());
  }
};

export const getUserById: RequestHandler = async (req, res, next) => {
  const { id } = req.params;
  try {
    const user = await userService.getUserById(id);
    if (!user) return next(new UrlNotFoundException(req.originalUrl));

    const response: ResponseMessage = {
      status: 200,
      message: "Success",
      data: { user },
    };

    res.status(response.status).json(response);
  } catch (err) {
    next(new InternalServerException());
  }
};

export const createUser: RequestHandler = async (req, res, next) => {
  const userDto: ICreateUserDto = req.body;
  try {
    const user = await userService.createUser(userDto);

    const response: ResponseMessage = {
      status: 201,
      message: "Created Successfully",
      data: { user },
    };

    res.status(response.status).json(response);
  } catch (err) {
    next(new InternalServerException());
  }
};

export const updateUser: RequestHandler = async (req, res, next) => {
  const { id } = req.params;
  const userDto: IUpdateUserDto = req.body;
  try {
    const user = await userService.updateUser(id, userDto);
    if (!user) return next(new UrlNotFoundException(req.originalUrl));

    const response: ResponseMessage = {
      status: 204,
      message: "Updated Successfully",
      data: { user },
    };

    res.status(response.status).json(response);
  } catch (err) {
    next(new InternalServerException());
  }
};

export const deleteUser: RequestHandler = async (req, res, next) => {
  const { id } = req.params;
  try {
    const user = await userService.deleteUser(id);
    if (!user) return next(new UrlNotFoundException(req.originalUrl));

    const response: ResponseMessage = {
      status: 204,
      message: "Deleted Successfully",
      data: { user },
    };

    res.status(response.status).json(response);
  } catch (err) {
    next(new InternalServerException());
  }
};
