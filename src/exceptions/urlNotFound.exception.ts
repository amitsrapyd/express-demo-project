import { HttpException } from "./http.exception.js";

export class UrlNotFoundException extends HttpException {
  constructor(public url: string) {
    super(`Url with path ${url} not found`, 404);
  }
}
