import { HttpException } from "./http.exception.js";

export class InternalServerException extends HttpException {
  constructor() {
    super("Internal Server Error", 500);
  }
}
