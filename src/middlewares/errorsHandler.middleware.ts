import { NextFunction, Request, RequestHandler, Response } from "express";
import fs from "fs";
import { HttpException } from "../exceptions/http.exception.js";
import { UrlNotFoundException } from "../exceptions/urlNotFound.exception.js";
import { ErrorMessage } from "../types/messages.interface.js";

type ErrorMiddleware = (
  error: HttpException,
  req: Request,
  res: Response,
  next: NextFunction
) => ReturnType<RequestHandler>;

export const errorLogger = (filePath: string): ErrorMiddleware => {
  const writeStream = fs.createWriteStream(filePath, {
    encoding: "utf-8",
    flags: "a",
  });

  return function (err, req, res, next) {
    const { requestId } = req;
    const { message, status, stack } = err;
    writeStream.write(`${requestId} - ${status} :: ${message} >> ${stack}\n`);

    next(err);
  };
};

export const printError: ErrorMiddleware = (err, req, res, next) => {
  const { requestId } = req;
  const { message, status, stack } = err;
  console.log(`${requestId} - ${status} :: ${message} ${stack}\n`);

  next(err);
};

export const urlNotFound: RequestHandler = (req, res, next) => {
  next(new UrlNotFoundException(req.originalUrl));
};

export const errorReponse: ErrorMiddleware = (err, req, res, next) => {
  const { status, message, stack } = err;
  const errorReponse: ErrorMessage = { message, status };

  if (process.env.NODE_ENV === "development") {
    errorReponse.stack = stack;
  }

  res.status(status).json(errorReponse);
};
