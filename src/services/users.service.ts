import userModel from "../models/user.model.js";
import { ICreateUserDto, IUpdateUserDto } from "../types/user.interface.js";
import { generateId } from "../utils/generate-id.js";

class UserService {
  async getAllUsers() {
    const users = await userModel.read();
    return users;
  }

  async getUserById(userId: string) {
    const users = await userModel.read();
    const user = users.find((user) => user.id === userId);

    return user;
  }

  async createUser(userDto: ICreateUserDto) {
    const users = await userModel.read();
    const user = { ...userDto, id: generateId() };
    users.push(user);
    await userModel.save(users);
    return user;
  }

  async updateUser(userId: string, userDto: IUpdateUserDto) {
    const users = await userModel.read();
    const userIndex = users.findIndex((user) => user.id === userId);
    if (userIndex === -1) return null;

    users[userIndex] = {
      ...users[userIndex],
      ...userDto,
    };

    await userModel.save(users);

    return users[userIndex];
  }

  async deleteUser(userId: string) {
    const users = await userModel.read();
    const userIndex = users.findIndex((user) => user.id === userId);
    if (userIndex === -1) return null;

    const user = users.splice(userIndex, 1);

    await userModel.save(users);

    return user;
  }
}

const instance = new UserService();

export default instance;
