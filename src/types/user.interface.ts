export interface IUser {
  id: string;
  username: string;
  password: string;
}

export type ICreateUserDto = Omit<IUser, "id">;

export type IUpdateUserDto = Partial<ICreateUserDto>;
