import express from "express";
import morgan from "morgan";
import path from "path";
import log from "@ajar/marker";
import apiRouter from "./routes/api.router.js";
import webRouter from "./routes/web.router.js";
import { logger } from "./middlewares/logger.middleware.js";
import {
  errorLogger,
  errorReponse,
  printError,
  urlNotFound,
} from "./middlewares/errorsHandler.middleware.js";
import { attachRequestId } from "./middlewares/attachRequestId.middleware.js";

const { PORT, HOST } = process.env;

async function startServer() {
  const REQUESTS_LOG_PATH = path.join(process.cwd(), "logs", "requests.log");
  const ERRORS_LOG_PATH = path.join(process.cwd(), "logs", "errors.log");

  const app = express();

  app.use(attachRequestId);
  app.use(morgan("dev"));
  app.use(logger(REQUESTS_LOG_PATH));
  app.use(express.json());

  app.use("/api", apiRouter);
  app.use("/", webRouter);

  app.use(urlNotFound);
  app.use(printError);
  app.use(errorLogger(ERRORS_LOG_PATH));
  app.use(errorReponse);

  app.listen(Number(PORT), HOST as string, () => {
    log.magenta("🌎  listening on", `http://${HOST}:${PORT}`);
  });
}

startServer();
